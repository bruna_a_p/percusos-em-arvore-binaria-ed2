package utfpr.dainf.ct.ed.exemplo;

import java.util.ArrayDeque;
import java.util.ArrayList;

import java.util.Stack;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós na árvore
 */
public class ArvoreBinaria<E> {
    
    protected E valor;
    protected ArvoreBinaria<E> pai;
    protected ArvoreBinaria<E> esquerda;
    protected ArvoreBinaria<E> direita;
    
    // para percurso iterativo
    private boolean inicio = true;
    private boolean visitado = false;
    private boolean fim = false;
    private Stack<ArvoreBinaria<E>> pilha;
    private ArrayDeque<ArvoreBinaria<E>> fila;
    private ArrayList<ArvoreBinaria<E>> lista;
    private ArvoreBinaria<E> ultimoVisitado;
    private ArvoreBinaria<E> noPos;

    /**
     * Cria uma árvore binária com valor nulo na raiz.
     */
    public ArvoreBinaria() {
    }

    /**
     * Cria uma árvore binária com valor {@code valor} na raiz.
     * @param valor O valor do nó raiz
     */
    public ArvoreBinaria(E valor) {
        this.valor = valor;
    }
    
    /**
     * Insere uma subárvore à esquerda deste nó.
     * A subárvore à esquerda deste nó é inserida na folha mais à esquerda
     * da subárvore inserida.
     * @param a A subárvore a ser inserida.
     * @return A subárvore inserida.
     */
    public ArvoreBinaria<E> insereEsquerda(ArvoreBinaria<E> a) {
        ArvoreBinaria<E> e = esquerda;
        ArvoreBinaria<E> x = a;
        esquerda = a;
        esquerda.pai = this;
        while (x.esquerda != null) //aqui ele se prepara para receber outro nó, porque pode ser tanto um nó como uma subárvore
            x = x.esquerda;
        x.esquerda = e;
        return a;
    }
    
    /**
     * Insere uma subárvore à direita deste nó.
     * A subárvore à direita deste nó é inserida na folha mais à direita
     * da subárvore inserida.
     * @param a A subárvore a ser inserida.
     * @return A subárvore inserida.
     */
    public ArvoreBinaria<E> insereDireita(ArvoreBinaria<E> a) {
        ArvoreBinaria<E> d = direita;
        ArvoreBinaria<E> x = a;
        direita = a;
        direita.pai = this;
        while (x.direita != null)
            x = x.direita;
        x.direita = d;
        return a;
    }
    
    /**
     * Implementação padrão que exibe o valor armazenado no nó usando
     * o método {@code toString()}.
     * Pode ser sobrecarregado em classes derivadas para implementar outras
     * formas de visita.
     * @param no O nó a ser visitado
     */
    protected void visita(ArvoreBinaria<E> no) {
        System.out.print(" " + no.valor);
    }
    
    /**
     * Visita os nós da subárvore em-ordem.
     * @param raiz A raiz da subárvore
     */
    public void visitaEmOrdem(ArvoreBinaria<E> raiz) {
        if (raiz != null) {
            visitaEmOrdem(raiz.esquerda);
            visita(raiz);
            visitaEmOrdem(raiz.direita);
        }
    }
    
    /**
     * Visita os nós da árvore em-ordem a partir da raiz.
     */
    public void visitaEmOrdem() {
        visitaEmOrdem(this);
    }
    
    /** OK
     * Visita os nós da subárvore em pre-ordem.
     * @param raiz A raiz da subárvore
     */
    public void visitaPreOrdem(ArvoreBinaria<E> raiz) {
         if (raiz != null) {
            visita(raiz);
            visitaPreOrdem(raiz.esquerda);
            visitaPreOrdem(raiz.direita);
        }
    }
    
    /**
     * Visita os nós da árvore em pre-ordem a partir da raiz.
     */
    public void visitaPreOrdem() {
        visitaPreOrdem(this);
    }
    
    /** OK
     * Visita os nós da subárvore em pre-ordem.
     * @param raiz A raiz da subárvore
     */
    public void visitaPosOrdem(ArvoreBinaria<E> raiz) {
        if (raiz != null) {
            visitaPosOrdem(raiz.esquerda);
            visitaPosOrdem(raiz.direita);
            visita(raiz);
        }
    }
    
    /**
     * Visita os nós da árvore em pre-ordem a partir da raiz.
     */
    public void visitaPosOrdem() {
        visitaPosOrdem(this);
    }
    
    private void inicializaPilha() {
        if (pilha == null) {
            pilha = new Stack<>();
        }
    }
    
    private void inicializaFila() {
        if (fila == null) {
            fila = new ArrayDeque<>();
        }
    }
    
    private void inicializaLista() {
        if (lista == null) {
            lista =  new ArrayList<>();
        }
    }
    
    /**
     * Reinicia o percurso a partir do início.
     * Deve ser chamado após percorrer toda a árvore para realizar novo
     * percurso ou para voltar ao início a qualquer momento.
     */
    public void reinicia() {
        inicializaPilha();
        inicializaFila();
        inicializaLista();
        pilha.clear();
        fila.clear();
        lista.clear(); // SERÁ QUE AQUI ESTÁ DANDO PROBLEMA?????????????????????????????????????????????????????????????
        ultimoVisitado = this; // aqui o this é a raiz porque ele chama por a, então this é a
        inicio = true;
        fim = false;
    }
    
    /**
     * Retorna o valor do próximo nó em-ordem.
     * @return O valor do próximo nó em-ordem.
     */
    public ArvoreBinaria<E> proximoEmOrdem() {
        ArvoreBinaria<E> resultado = null;
        if (inicio) {
            reinicia();
            inicio = false;
        }
        if (!pilha.isEmpty() || ultimoVisitado != null) {
            while (ultimoVisitado != null) {
                pilha.push(ultimoVisitado);
                ultimoVisitado = ultimoVisitado.esquerda;
            }
            ultimoVisitado = pilha.pop();
            resultado = ultimoVisitado;
            ultimoVisitado = ultimoVisitado.direita;
        }
        else
            inicio = true;
        
        return resultado;
    }
    
    /** OK
     * Retorna o próximo nó em pré-ordem.
     * @return O próximo nó em pré-ordem.
     */
    public ArvoreBinaria<E> proximoPreOrdem()
    {
        ArvoreBinaria<E> resultado = null;
        
        //inicializando a pilha, faz this virar a raiz
        if (inicio) {
            reinicia();
            inicio = false;
        }
        //colocando os nós na pilha
        if ( !lista.isEmpty() || ultimoVisitado != null )
        {
            if( fim != true )
            {
                lista.add(this); // adicionamos mas ainda não marcamos
                // no primeiro while vamos percorrer a subárvore da esquerda, depois a da esquerda, e por fim a raiz
                while ( this.esquerda != null && (this.visitado != true) ) // verificando se existe uma subárvore na esquerda
                { 
                    //primiro verificamos a esquerda e depois a direita, e assim vamos andando
                    if( ultimoVisitado.esquerda != null && ultimoVisitado.esquerda.visitado != true ) // descendo pela esquerda
                    {
                        ultimoVisitado = ultimoVisitado.esquerda;
                        ultimoVisitado.visitado = true; //andei e já marquei
                        lista.add(ultimoVisitado); // colocando o nó na fila
                    }
                    else if( ultimoVisitado.direita != null && ultimoVisitado.direita.visitado != true ) // descendo pela direita
                    {
                        ultimoVisitado = ultimoVisitado.direita;
                        ultimoVisitado.visitado = true; //andei e já marquei
                        lista.add(ultimoVisitado); // colocando o nó na fila
                    }
                    // e se o nó for a folha ou tiver os filhos marcados? Não podemos marcar o próximo nó, então vamos subir
                    else if( (ultimoVisitado.direita == null || ultimoVisitado.direita.visitado == true ) && (ultimoVisitado.esquerda == null || ultimoVisitado.esquerda.visitado == true) )
                    {
                        ultimoVisitado = ultimoVisitado.pai; // a raiz é o único nó PAI que não foi marcado, porque´precisamos marcar os pais antes de acessar os filhos
                        ultimoVisitado.visitado = true; // como todos os pais já foram marcados, ele vai parar quando chegar na raiz, mas não vai adicioná-la na lista
                        if( this.direita.visitado != true )
                            this.visitado = false;
                    }
                }
            }
            //já saiu do while, acho que não entra mais, então vai passar direto pra cá
            fim = true;
            if(!lista.isEmpty())
            {
                ultimoVisitado = lista.get(0);
                lista.remove(0);
                if( ultimoVisitado != null )
                    ultimoVisitado.visitado = false;
                resultado = ultimoVisitado;
            }
            else
            {
                inicio = true;
                ultimoVisitado = null;
            }
        }
        //retornando o próximo nó
        return resultado;
    }
    
    /** OK
     * Retorna o próximo nó em pós-ordem.
     * @return O próximo nó em pós-ordem.
     */
    public ArvoreBinaria<E> proximoPosOrdem() {
        ArvoreBinaria<E> resultado = null;
        
        //inicializando a pilha, faz this virar a raiz
        if (inicio) {
            reinicia();
            inicio = false;
        }
        //colocando os nós na pilha
        if ( !lista.isEmpty() || ultimoVisitado != null)
        {
            while ( (this.visitado != true) && (fim == false) )
            { /*entra aqui quando a pilha não está vazia ou o último nó visitado não é nulo*/
                //andando pela árvore
                if( ultimoVisitado.esquerda != null && ultimoVisitado.esquerda.visitado != true )
                    ultimoVisitado = ultimoVisitado.esquerda;
                else if( ultimoVisitado.direita != null && ultimoVisitado.direita.visitado != true ) // só vem pra direita quando não há esquerda disponível
                    ultimoVisitado = ultimoVisitado.direita;
                //decidindo o que fazer como nó, só fazemos açgo se é a vez dele de ser marcado
                //se teve os filhos visitados ou for a raiz
                else if( (ultimoVisitado.direita == null || ultimoVisitado.direita.visitado == true) && (ultimoVisitado.esquerda == null || ultimoVisitado.esquerda.visitado == true) )
                {
                    ultimoVisitado.visitado = true;
                    lista.add(ultimoVisitado); // colocando o nó na fila
                    if( ultimoVisitado.pai != null ) // se não for a raíz
                        ultimoVisitado = ultimoVisitado.pai;
                }
            }
            //já saiu do while, acho que não entra mais, então vai passar direto pra cá
            fim = true;
            if(!lista.isEmpty())
            {
                ultimoVisitado = lista.get(0);
                lista.remove(0);
                if( ultimoVisitado != null )
                    ultimoVisitado.visitado = false;
                resultado = ultimoVisitado;
            }
            else
            {
                inicio = true;
                ultimoVisitado = null;
            }
        }
        //retornando o próximo nó
        return resultado;
    }
    
    /** OK
     * Retorna o próximo nó em nível.
     * @return O próximo nó em nível.
     */
    public ArvoreBinaria<E> proximoEmNivel()
    {
        ArvoreBinaria<E> resultado = null;
        
        if (inicio) {
            reinicia();
            inicio = false;
        }
        
        if( fim != true )
        {
            lista.add(this); // agora nossa listá já tem um elemento

            if( this.esquerda != null )
                lista.add( this.esquerda );

            if( this.direita != null ) // conferindo se existem nós à esquerda do atual da lista
                lista.add( this.direita );

            for( int i = 1 ; i < lista.size() ; i++ ) // vamos começar no segundo nó porque já colocamos os filhos do primeiro
            {
                ultimoVisitado = lista.get(i); // pega o elemento atual da lista para colocar as coisas no vetor
                
                //adicionando o nó da esquerda, apenas se não for nulo
                if( ultimoVisitado.esquerda != null ) // conferindo se existem nós à esquerda do atual da lista
                    lista.add( ultimoVisitado.esquerda );
                
                //adicionando o nó da direita, apenas se não for nulo
                if( ultimoVisitado.direita != null ) // conferindo se existem nós à esquerda do atual da lista
                    lista.add( ultimoVisitado.direita );

            }
            fim = true;
        }
        if(!lista.isEmpty())
        {
            ultimoVisitado = lista.get(0);
            lista.remove(0);
            if( ultimoVisitado != null )
                ultimoVisitado.visitado = false;
            resultado = ultimoVisitado;
        }
        else
        {
            inicio = true;
            ultimoVisitado = null;
        }
        
        return resultado;
    }
    
    /**
     * Retorna o valor armazenado no nó.
     * @return O valor armazenado no nó.
     */
    public E getValor() {
        return valor;
    }

    /**
     * Atribui um valor ao nó.
     * @param valor O valor a ser atribuído ao nó.
     */
    public void setValor(E valor) {
        this.valor = valor;
    }

    /**
     * Retorna a árvore esqueda.
     * @return A árvore esquerda.
     */
    protected ArvoreBinaria<E> getEsquerda() {
        return esquerda;
    }

    /**
     * Retorna a árvore direita.
     * @return A árvore direita.
     */
    protected ArvoreBinaria<E> getDireita() {
        return direita;
    }

    /**
     * Inicializa a árvore esquerda.
     * @param esquerda A árvore esquerda.
     */
    protected void setEsquerda(ArvoreBinaria<E> esquerda) {
        this.esquerda = esquerda;
    }

    /**
     * Inicializa a árvore direita.
     * @param direita A árvore direita.
     */
    protected void setDireita(ArvoreBinaria<E> direita) {
        this.direita = direita;
    }
    
}
